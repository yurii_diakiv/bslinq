﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BSLINQ
{
    class Project
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime created_at { get; set; }
        public DateTime deadline { get; set; }
        public int? author_id { get; set; }
        public int? team_id { get; set; }

        public override string ToString()
        {
            return $"id : {id}\nname : {name}\ndescription : {description}\ncreated_at : {created_at}\ndeadline : {deadline}\n" +
                $"author_id : {author_id}\nteam_id : {team_id}\n";
        }
    }
}
